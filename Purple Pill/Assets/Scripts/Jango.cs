using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jango : MonoBehaviour
{
    private bool jumpkeyWasPressed;
    private float horizontalInput;
    private Rigidbody rigitbodyComponent;
    public Transform terrainCheckTransform = null;
    public LayerMask jangoMask;



    // Start is called before the first frame update
    void Start()
    {
        rigitbodyComponent = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            jumpkeyWasPressed = true;
        }

        horizontalInput = Input.GetAxis("Horizontal");
    }

    //FixedUpdate is called once every physic update
    private void FixedUpdate()
    {
        rigitbodyComponent.velocity = new Vector3(horizontalInput, rigitbodyComponent.velocity.y, 0);

        if (Physics.OverlapSphere(terrainCheckTransform.position, 0.1f, jangoMask).Length == 0)
        {
            return;
        }


        if (jumpkeyWasPressed)
        {
            rigitbodyComponent.AddForce(Vector3.up * 5, ForceMode.VelocityChange);
            jumpkeyWasPressed = false;
        }

    }
}
